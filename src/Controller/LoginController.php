<?php
namespace CmsAdmin\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;

class LoginController extends AbstractActionController {
    
    public function loginAction() {

        if($this->identity()) {
            return $this->redirect()->toRoute('cms-admin'); 
        }

        $return = array('post' => null);

        $auth = $this->plugin('cms.extension.manager')->get('auth-manager');
        
        $request = $this->getRequest();

        if($request->isPost()) {
            $post = $request->getPost();
            
            $result = $auth->login($post);
            
            if($result->isvalid()) {
                
                return $this->redirect()->toRoute('cms-admin');

            } else {
                $messages = $result->getMessages();
                var_dump($messages);die('messages');
            
            }


            $return['post'] = $request->getPost();
        }



        return $return;
    }

    public function logoutAction() {

        $return = array('post' => null);

        $auth = $this->plugin('cms.extension.manager')->get('auth-manager');
        
        $auth->logout();


        return $this->redirect()->toRoute('cms-login');
    }
} 