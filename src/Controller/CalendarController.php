<?php
namespace CmsAdmin\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;

class CalendarController extends AbstractActionController {

    public function indexAction() {
        
        $auth = $this->plugin('cms.extension.manager')->get('auth-manager');

        $identity = $this->identity();
        
        $page = array("datapage" => "calendar",
                      "username" => $identity['username']);

        $this->layout()->setVariables($page);

        return array('identity' => $identity);
    }
} 