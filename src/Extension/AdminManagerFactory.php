<?php
namespace CmsAdmin\Extension;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class AdminManagerFactory implements FactoryInterface {

    public function createService(ServiceLocatorInterface $serviceLocator) {
    
        return new AdminManager();
    }
}