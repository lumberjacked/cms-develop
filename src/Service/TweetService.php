<?php

namespace Cms\Develop\Service;

use Zend\EventManager\EventManager;
use Zend\EventManager\EventManagerAwareInterface;
use Zend\EventManager\EventManagerInterface;

class TweetService implements EventManagerAwareInterface
{
    /**
     * @var EventManagerInterface
     */
    protected $eventManager;

    /**
     * @param string $content
     */
    public function sendTweet($content)
    {
        // Send the tweet with Twitter's API...
    	
        // Trigger an event
        $collection = $this->getEventManager()->trigger('tweet-event', null, array('content' => $content));
        return $collection->last();
    }

    /**
     * @param  EventManagerInterface $eventManager
     * @return void
     */
    public function setEventManager(EventManagerInterface $eventManager) {
	    $eventManager->addIdentifiers(array(
	        
	        get_called_class()
	    ));

	    $this->eventManager = $eventManager;
	}

    /**
     * @return EventManagerInterface
     */
    public function getEventManager()
    {
        if (null === $this->eventManager) {
            $this->setEventManager(new EventManager());
        }

        return $this->eventManager;
    }
}