<?php

return array(

    'controllers' => include 'controller.config.php',

    'router' => include 'router.config.php',

    'service_manager' => include 'service.config.php',

    'view_manager' => array(
        'template_map' => array(
            'cms-admin/admin/dashboard'  => __DIR__ . '/../view/cms-admin/admin/dashboard.phtml',
            'cms-admin/login/login'      => __DIR__ . '/../view/cms-admin/login/login.phtml',
            'cms-admin/profile/index'    => __DIR__ . '/../view/cms-admin/admin/profile/index.phtml',
            'cms-admin/profile/settings' => __DIR__ . '/../view/cms-admin/admin/profile/settings.phtml',
            'cms-admin/calendar/index'   => __DIR__ . '/../view/cms-admin/admin/calendar/index.phtml',
            'cms-admin/messages/index'   => __DIR__ . '/../view/cms-admin/admin/messages/index.phtml',
            'cms-admin/notifications/index'   => __DIR__ . '/../view/cms-admin/admin/notifications/index.phtml'
        )
    ),

);