<?php 

return array(
    'routes' => array(
        'zf-apigility' => array(
            'type'  => 'Zend\Mvc\Router\Http\Literal',
            'options' => array(
                'route' => '/apigility',
            ),
            'may_terminate' => false,
            'child_routes' => array(
                'developer' => array(
                    'type' => 'Zend\Mvc\Router\Http\Segment',
                    'options' => array(
                        'route'    => '/developer[/:action]',
                        'defaults' => array(
                            'controller' => 'cms.controller.dev',
                            'action'     => 'index',
                        ),
                    ),
                ),
            ),
        ),
        'cms-dev' => array(
            'type' => 'Zend\Mvc\Router\Http\Segment',
            'options' => array(
                'route'    => '/developer[/:action]',
                'defaults' => array(
                    'controller' => 'cms.controller.dev',
                    'action'     => 'index',
                ),
            ),
        )
    )
);



